import * as fromAlarms from "./AlarmReducer";
import * as fromDate from "./DateReducer";
import { combineReducers } from "redux";

export const initialState: State = {
  alarms: fromAlarms.initialState,
  currentDate: fromDate.initialState
};

export interface State {
  alarms: fromAlarms.State;
  currentDate: fromDate.State;
}

export const reducer = combineReducers<State>({
  currentDate: fromDate.reducer,
  alarms: fromAlarms.reducer
});
