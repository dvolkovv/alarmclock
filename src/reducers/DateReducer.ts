import { SetDateAction, ActionTypes } from "../actions/DateAction";

export const initialState: State = {
  currentDate: new Date().toLocaleDateString()
};

export interface State {
  currentDate: string;
}

export function reducer(state: State = initialState, action: SetDateAction) {
  switch (action.type) {
    case ActionTypes.SET_CURRENT_DATE: {
      const dateString = action.dateString;
      return Object.assign({}, state, { ...state, currentDate: dateString });
    }
    default: {
      return state;
    }
  }
}
