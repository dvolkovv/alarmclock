import Alarm from "../models/Alarm";
import { Action, ActionTypes } from "../actions/AlarmActions";
import { getAlarmTime } from "../models/Alarm";

export const initialState: State = {
  alarms: []
};

export interface State {
  alarms: Alarm[];
}

export function reducer(state: State = initialState, action: Action) {
  switch (action.type) {
    case ActionTypes.ADD_ALARM: {
      let newId;
      //set last id
      if (state.alarms.length == 0) {
        newId = 0;
      } else {
        newId = state.alarms[state.alarms.length - 1].id.valueOf() + 1;
      }
      action.alarm.id = newId;
      return Object.assign({}, state, {
        alarms: state.alarms.concat(action.alarm)
      });
    }
    case ActionTypes.TOGGLE_ALARM: {
      const alarmId = action.alarmId;
      return {
        ...state,
        alarms: state.alarms.map(alarm =>
          alarm.id === alarmId ? { ...alarm, isActive: !alarm.isActive } : alarm
        )
      };
    }
    case ActionTypes.CHANGE_ALARM_TIME: {
      const id = action.payload.alarmId;
      const newTime = action.payload.time;
      return {
        ...state,
        alarms: state.alarms.map(alarm =>
          alarm.id === id ? { ...alarm, time: newTime } : alarm
        )
      };
    }
    case ActionTypes.CHANGE_ALARM_COMMENT: {
      const id = action.payload.alarmId;
      const newComment = action.payload.comment;
      return {
        ...state,
        alarms: state.alarms.map(alarm =>
          alarm.id === id ? { ...alarm, comment: newComment } : alarm
        )
      };
    }

    default: {
      return state;
    }
  }
}
