export function isDayTime(time: Date): boolean {
  let hours = time.getHours();
  if (hours > 6 && hours < 18) {
    return true;
  }
  return false;
}
