export const isDatesEqual = (date1: Date, date2: Date) => {
  if (
    date1.getDay() === date2.getDay() &&
    date1.getMonth() === date2.getMonth() &&
    date1.getFullYear() === date2.getFullYear()
  ) {
    return true;
  } else return false;
};

export const getNextDate = (currentDate: Date) => {
  let nextDate = new Date(currentDate.toISOString());
  nextDate.setDate(nextDate.getDate() + 1);
  return nextDate;
};

export const getPrevDate = (currentDate: Date) => {
  let prevDate = new Date(currentDate.toISOString());
  prevDate.setDate(prevDate.getDate() - 1);
  return prevDate;
};
