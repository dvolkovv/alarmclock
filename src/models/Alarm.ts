export default interface Alarm {
  id: Number;
  time: string;
  comment: String;
  isActive: boolean;
}

export function setAlarmTime(alarm: Alarm, newTime: Date) {
  const newDate = new Date(alarm.time);
  newDate.setHours(newTime.getHours(), newTime.getMinutes());
  alarm.time = newDate.toLocaleString();
}

export function getAlarmTime(alarm: Alarm): Date {
  return new Date(alarm.time);
}

export function getAlarmTimeString(alarm: Alarm): String {
  let resultDate = new Date(alarm.time);
  resultDate.setSeconds(0);
  return resultDate.toLocaleTimeString("ru", { hour12: false });
}
