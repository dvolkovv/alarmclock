import { State } from "../reducers/RootReducer";
import { createSelector } from "reselect";
import { getDate } from "./DateSelector";
import { getAlarmTime } from "../models/Alarm";
import { isDatesEqual } from "../utils/DateUtils";

const getAlarmsState = (state: State) => {
  return state.alarms;
};

export const getAlarmsByCurrentDate = createSelector(
  [getAlarmsState, getDate],
  (alarms, date) => {
    const currentDate = new Date(date.currentDate);
    return alarms.alarms.filter(item =>
      isDatesEqual(getAlarmTime(item), currentDate)
    );
  }
);

export const getAllAlarms = createSelector(
  [getAlarmsState],
  alarms => {
    return alarms.alarms;
  }
);
