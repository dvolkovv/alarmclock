import { State } from "../reducers/RootReducer";
import { createSelector } from "reselect";

export const getDate = (state: State) => state.currentDate;

export const getCurrentDate = createSelector(
  [getDate],
  s => new Date(s.currentDate)
);
