import React from "react";
import CalendarPicker from "react-native-calendar-picker";
import { Props } from "./containers/CalendarContainer";

interface State {}
export default class Calendar extends React.Component<Props, State> {
  render() {
    return (
      <CalendarPicker
        startFromMonday={true}
        weekdays={["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]}
        months={[
          "Январь",
          "Февраль",
          "Март",
          "Апрель",
          "Май",
          "Июнь",
          "Июль",
          "Август",
          "Сентябрь",
          "Октябрь",
          "Ноябрь",
          "Декабрь"
        ]}
        previousTitle="Назад"
        nextTitle="Вперед"
        initialDate={this.props.currentDate}
        minDate={Date()}
        onDateChange={this.props.onDateSelected}
        selectedDayColor="#457CE3"
      />
    );
  }
}
