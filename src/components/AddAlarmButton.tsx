import React from "react";
import { StyleSheet, View, Button, ToastAndroid } from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";

interface Props {
  addAlarm: (time: Date) => void;
  currentDate: Date;
}
interface State {
  isTimePickerOpened: boolean;
  alarmTime: Date;
}
export default class AddAlarmButton extends React.Component<Props, State> {
  state: State = {
    isTimePickerOpened: false,
    alarmTime: new Date()
  };

  _validateTime = (time: Date) => {
    const res = time > new Date();
    if (!res) {
      ToastAndroid.show(
        "Нельзя установить будильник ранее текущего времени!",
        ToastAndroid.LONG
      );
    }
    return res;
  };

  _addAlarm = () => {
    const time = this.state.alarmTime;
    this.props.addAlarm(time);
  };

  _showTimePicker = () => this.setState({ isTimePickerOpened: true });

  _hideTimePicker = () => this.setState({ isTimePickerOpened: false });

  _handleTimePicked = (time: Date) => {
    this._hideTimePicker();
    var alarmTime = this.state.alarmTime;
    const date = this.props.currentDate;
    alarmTime.setFullYear(date.getFullYear(), date.getMonth(), date.getDate());
    alarmTime.setHours(time.getHours(), time.getMinutes());
    if (!this._validateTime(alarmTime)) {
      return;
    }
    this._addAlarm();
  };

  render() {
    return (
      <View style={styles.buttonContainer}>
        <Button onPress={this._showTimePicker} title={"Добавить напоминание"} />
        <DateTimePicker
          mode={"time"}
          isVisible={this.state.isTimePickerOpened}
          onConfirm={this._handleTimePicked}
          onCancel={this._hideTimePicker}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    marginVertical: 10,
    marginHorizontal: 30
  }
});
