import {
  FlatList,
  Text,
  View,
  StyleSheet,
  Switch,
  TextInput,
  SectionList
} from "react-native";
import React from "react";
import DateTimePicker from "react-native-modal-datetime-picker";
import { ToastAndroid } from "react-native";
import Alarm, {
  setAlarmTime,
  getAlarmTimeString,
  getAlarmTime
} from "../models/Alarm";
import { isDayTime } from "../utils/TimeUtils";
import {
  AlarmListOwnProps,
  AlarmListConnectedProps
} from "./containers/AlarmListContainer";

interface State {
  isTimePickerOpened: boolean;
  timePickerTime?: Date;
  changedAlarm?: Alarm;
}
export default class AlarmList extends React.Component<
  AlarmListOwnProps & AlarmListConnectedProps,
  State
> {
  state: State = {
    isTimePickerOpened: false,
    timePickerTime: undefined,
    changedAlarm: undefined
  };
  _toggleAlarm = (id: Number) => {
    this.props.toggleAlarm(id);
  };

  _changeAlarmTime = (time: Date) => {
    if (this.state.changedAlarm == undefined) {
      return;
    }
    if (!this._validateTime(time)) {
      return;
    }
    let currentAlarm = this.state.changedAlarm;
    setAlarmTime(currentAlarm, time);
    this.props.changeAlarmTime(
      currentAlarm.id.valueOf(),
      getAlarmTime(currentAlarm)
    );
  };

  _changeAlarmComment = (alarm: Alarm, comment: string) => {
    this.props.changeAlarmComment(alarm.id.valueOf(), comment);
  };

  _onAlarmTimeClick = (alarm: Alarm) => {
    this.state.timePickerTime = getAlarmTime(alarm);
    this.state.changedAlarm = alarm;
    this._showTimePicker();
  };

  _handleTimePicked = (time: Date) => {
    this._changeAlarmTime(time);
    this._hideTimePicker();
  };

  _renderItem = ({ item }: { item: Alarm }) => (
    <AlarmListItem
      onTimeClicked={this._onAlarmTimeClick}
      onSwitchChanged={this._toggleAlarm}
      onEdit={(comment: string) => {
        this._changeAlarmComment(item, comment);
      }}
      alarm={item}
    />
  );

  _validateTime = (time: Date) => {
    const res = time > new Date();
    if (!res) {
      ToastAndroid.show(
        "Нельзя установить будильник ранее текущего времени!",
        ToastAndroid.LONG
      );
    }
    return res;
  };

  _showTimePicker = () => this.setState({ isTimePickerOpened: true });

  _hideTimePicker = () => this.setState({ isTimePickerOpened: false });

  _getDayTimeAlarms = () =>
    this.props.alarms.filter(item => isDayTime(getAlarmTime(item)));

  _getNightTimeAlarms = () =>
    this.props.alarms.filter(item => !isDayTime(getAlarmTime(item)));

  render() {
    return (
      <View>
        <DateTimePicker
          mode="time"
          isVisible={this.state.isTimePickerOpened}
          date={this.state.timePickerTime}
          onConfirm={this._handleTimePicked}
          onCancel={this._hideTimePicker}
        />
        <SectionList
          sections={[
            { title: "Утро", data: this._getDayTimeAlarms() },
            { title: "Вечер", data: this._getNightTimeAlarms() }
          ]}
          contentContainerStyle={styles.list}
          renderItem={this._renderItem}
          keyExtractor={(index: number) => index.toString()}
        />
      </View>
    );
  }
}

interface Prop {
  alarm: Alarm;
  onSwitchChanged: (id: Number) => void;
  onTimeClicked: (alarm: Alarm) => void;
  onEdit: (comment: string) => void;
}

class AlarmListItem extends React.Component<Prop> {
  _toggleAlarm = () => {
    this.props.onSwitchChanged(this.props.alarm.id);
  };

  _onTimeClicked = () => {
    this.props.onTimeClicked(this.props.alarm);
  };

  render() {
    return (
      <View style={styles.rootItem}>
        <View>
          <Text style={styles.textStyle} onPress={this._onTimeClicked}>
            {getAlarmTimeString(this.props.alarm)}
          </Text>
          <TextInput
            defaultValue={this.props.alarm.comment.valueOf()}
            maxLength={120}
            onChangeText={this.props.onEdit}
          />
        </View>
        <Switch
          onValueChange={this._toggleAlarm}
          value={this.props.alarm.isActive}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rootItem: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  textStyle: {
    paddingTop: 8,
    fontSize: 16
  },
  list: {
    backgroundColor: "#ffff",
    elevation: 3,
    paddingHorizontal: 8,
    marginLeft: 16,
    marginRight: 16
  }
});
