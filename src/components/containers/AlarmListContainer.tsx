import { connect } from "react-redux";
import AlarmList from "../AlarmList";
import Alarm from "../../models/Alarm";
import { getAllAlarms } from "../../selectors/AlarmSelector";
import { isDatesEqual } from "../../utils/DateUtils";
import { getAlarmTime } from "../../models/Alarm";
import {
  toggleAlarm,
  changeAlarmTime,
  changeAlarmComment
} from "../../actions/AlarmActions";

export interface AlarmListOwnProps {
  date: Date;
  alarms: Alarm[];
}

export interface AlarmListConnectedProps {
  toggleAlarm: (id: Number) => void;
  changeAlarmTime: (id: number, time: Date) => void;
  changeAlarmComment: (id: number, newComment: String) => void;
}

const mapStateToProps = (state: any, props: AlarmListOwnProps) => {
  console.log("AlarmListProps : " + JSON.stringify(props));
  const dateAlarms = _filterAlarmsByDate(getAllAlarms(state), props.date);
  return {
    alarms: dateAlarms
  };
};

const mapDispatchToProps = {
  toggleAlarm: toggleAlarm,
  changeAlarmTime: changeAlarmTime,
  changeAlarmComment: changeAlarmComment
};

const _filterAlarmsByDate = (alarms: Alarm[], date: Date) => {
  return alarms.filter((alarm: Alarm) =>
    isDatesEqual(getAlarmTime(alarm), date)
  );
};

export default connect<any, any, any>(
  mapStateToProps,
  mapDispatchToProps
)(AlarmList);
