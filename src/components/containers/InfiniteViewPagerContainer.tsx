import { connect } from "react-redux";
import { getCurrentDate } from "../../selectors/DateSelector";
import InfiniteViewPager from "../InfiniteViewPager";
import { setCurrentDate } from "../../actions/DateAction";
import { getNextDate, getPrevDate } from "../../utils/DateUtils";

export interface Props {
  prevDate: Date;
  nextDate: Date;
  currentDate: Date;
  setCurrentDate: (date: Date) => void;
}

const mapStateToProps = (state: any) => {
  const currentDate = getCurrentDate(state);
  return {
    currentDate: currentDate,
    prevDate: getPrevDate(currentDate),
    nextDate: getNextDate(currentDate)
  };
};

const mapDispatchToProps = {
  setCurrentDate: setCurrentDate
};

export default connect<any, any, any>(
  mapStateToProps,
  mapDispatchToProps
)(InfiniteViewPager);
