import { connect } from "react-redux";
import Calendar from "../Calendar";
import { setCurrentDate } from "../../actions/DateAction";
import { getDate } from "../../selectors/DateSelector";

export interface Props {
  onDateSelected: (date: Date) => void;
  currentDate: Date;
}

const mapStateToProps = (state: any) => {
  return {
    currentDate: getDate(state)
  };
};

const mapDispatchToProps = {
  onDateSelected: setCurrentDate
};

export default connect<any, any, any>(
  mapStateToProps,
  mapDispatchToProps
)(Calendar);
