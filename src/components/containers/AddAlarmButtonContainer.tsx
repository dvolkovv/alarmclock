import { addAlarm } from "../../actions/AlarmActions";
import { connect } from "react-redux";
import AddAlarmButton from "../AddAlarmButton";
import { getCurrentDate } from "../../selectors/DateSelector";

const mapStateToProps = (state: any) => {
  return {
    currentDate: getCurrentDate(state)
  };
};

const mapDispatchToProps = {
  addAlarm: addAlarm
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddAlarmButton);
