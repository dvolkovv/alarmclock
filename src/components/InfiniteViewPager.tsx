import React from "react";
import {
  View,
  StyleSheet,
  ViewPagerAndroid,
  NativeSyntheticEvent,
  ViewPagerAndroidOnPageSelectedEventData,
  Text
} from "react-native";
import AlarmListContainer from "./containers/AlarmListContainer";
import { Props } from "./containers/InfiniteViewPagerContainer";

export default class InfiniteViewPager extends React.Component<Props> {
  _renderContent = (key: string, date: Date) => {
    console.log("ViewPager Key : " + key);
    return (
      <View key={key}>
        <AlarmListContainer date={date} />
      </View>
    );
  };

  _handleHorizontalScroll = (
    event: NativeSyntheticEvent<ViewPagerAndroidOnPageSelectedEventData>
  ) => {
    switch (event.nativeEvent.position) {
      case 0: {
        console.log("Setting prev date: " + this.props.prevDate.toDateString());
        this.props.setCurrentDate(this.props.prevDate);
        break;
      }
      case 2: {
        console.log("Setting next date: " + this.props.nextDate.toDateString());
        this.props.setCurrentDate(this.props.nextDate);
        break;
      }
    }
  };

  render() {
    const prevDate = this.props.prevDate;
    const nextDate = this.props.nextDate;
    const currentDate = this.props.currentDate;
    console.log(
      "CurrentViewPagerDate: " + this.props.currentDate.toDateString()
    );
    return (
      <ViewPagerAndroid
        onPageSelected={this._handleHorizontalScroll}
        initialPage={1}
        style={styles.root}
      >
        {this._renderContent(prevDate.toDateString(), prevDate)}
        {this._renderContent(currentDate.toDateString(), currentDate)}
        {this._renderContent(nextDate.toDateString(), nextDate)}
      </ViewPagerAndroid>
    );
  }
}

const styles = StyleSheet.create({
  root: { backgroundColor: "#F435", flex: 1 },
  pageStyle: { alignItems: "center", elevation: 3 }
});
