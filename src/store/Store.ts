import { createStore, applyMiddleware } from "redux";
import { reducer } from "../reducers/RootReducer";
import logger from "redux-logger";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import autoMergeLevel2 from "redux-persist/es/stateReconciler/autoMergeLevel2";

const persistConfig = {
  key: "root",
  storage: storage,
  stateReconciler: autoMergeLevel2,
  blacklist: ["currentDate"]
};

const pReducer = persistReducer(persistConfig, reducer);
const store = createStore(pReducer, applyMiddleware(logger));

export default store;
export const persistor = persistStore(store);
