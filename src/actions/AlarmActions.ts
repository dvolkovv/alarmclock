import Alarm from "../models/Alarm";

/*
 * Action definition
 */
export enum ActionTypes {
  ADD_ALARM = "ADD_ALARM",
  TOGGLE_ALARM = "TOGGLE_ALARM",
  CHANGE_ALARM_TIME = "CHANGE_ALARM_TIME",
  CHANGE_ALARM_COMMENT = "CHANGE_ALARM_COMMENT"
}

/*
 *Action interfaces
 */
export interface AddAlarmAction {
  type: ActionTypes.ADD_ALARM;
  alarm: Alarm;
}
export interface ToggleAlarmAction {
  type: ActionTypes.TOGGLE_ALARM;
  alarmId: number;
}
export interface ChangeAlarmTimeAction {
  type: ActionTypes.CHANGE_ALARM_TIME;
  payload: { alarmId: number; time: string };
}
export interface ChangeAlarmCommentAction {
  type: ActionTypes.CHANGE_ALARM_COMMENT;
  payload: { alarmId: number; comment: String };
}

/*
 * Action Creators
 */
export function addAlarm(date: Date): AddAlarmAction {
  const alarm: Alarm = {
    id: 0,
    time: date.toISOString(),
    comment: "",
    isActive: false
  };
  console.log("Added alarm " + JSON.stringify(alarm));
  return {
    type: ActionTypes.ADD_ALARM,
    alarm: alarm
  };
}

export function toggleAlarm(alarmId: number): ToggleAlarmAction {
  return { type: ActionTypes.TOGGLE_ALARM, alarmId };
}

export function changeAlarmTime(
  alarmId: number,
  newTime: Date
): ChangeAlarmTimeAction {
  return {
    type: ActionTypes.CHANGE_ALARM_TIME,
    payload: {
      alarmId: alarmId,
      time: newTime.toISOString()
    }
  };
}

export function changeAlarmComment(
  alarmId: number,
  newComment: String
): ChangeAlarmCommentAction {
  return {
    type: ActionTypes.CHANGE_ALARM_COMMENT,
    payload: {
      alarmId: alarmId,
      comment: newComment
    }
  };
}

/*
 * Action Type
 */
export type Action =
  | AddAlarmAction
  | ToggleAlarmAction
  | ChangeAlarmTimeAction
  | ChangeAlarmCommentAction;
