/*
 * Action definition
 */
export enum ActionTypes {
  SET_CURRENT_DATE = "SET_CURRENT_DATE"
}

/*
 *Action interfaces
 */
export interface SetDateAction {
  type: ActionTypes.SET_CURRENT_DATE;
  dateString: string;
}
/*
 * Action Creators
 */
export function setCurrentDate(date: Date): SetDateAction {
  const dateString = date.toISOString();
  return {
    type: ActionTypes.SET_CURRENT_DATE,
    dateString: dateString
  };
}
