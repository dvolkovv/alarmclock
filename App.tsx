import React, { Component } from "react";
import { Provider } from "react-redux";
import store from "./src/store/Store";
import AddAlarmButtonContainer from "./src/components/containers/AddAlarmButtonContainer";
import CalendarContainer from "./src/components/containers/CalendarContainer";
import { PersistGate } from "redux-persist/integration/react";
import { ActivityIndicator, View } from "react-native";
import { persistor } from "./src/store/Store";
import InfiniteViewPagerContainer from "./src/components/containers/InfiniteViewPagerContainer";

export default class App extends Component {
  _onAddClick = () => {};

  render() {
    return (
      <Provider store={store}>
        <PersistGate
          loading={<ActivityIndicator style={{ flex: 1 }} size="large" />}
          persistor={persistor}
        >
          <CalendarContainer />
          <AddAlarmButtonContainer />
          <InfiniteViewPagerContainer />
        </PersistGate>
      </Provider>
    );
  }
}
